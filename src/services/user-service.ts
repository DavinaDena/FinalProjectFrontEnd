import { getSession } from "next-auth/react";
import api from "./token-interceptor";



export async function fetchUserAccount() {
    // const session= await getSession()
    // console.log(session);

    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/user/account')
    
    return response
    
}


export async function userRegister(user: any){
    const response = await api.post(process.env.NEXT_PUBLIC_SERVER_URL+'/api/user/register', user)
    
    return response
}