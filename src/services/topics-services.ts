
import api from "./token-interceptor";

export async function fetchTopics(){
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/topic')
    return response.data

}

export async function fetchTopicById(id:any){
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/topic/'+id)
    //console.log(response.data);
    
    return response.data
}

export async function postTopicComment(id:any, comment:any){
    const response= await api.post(process.env.NEXT_PUBLIC_SERVER_URL+'/api/topic/'+id+'/comment', {text:comment})
    //console.log(response);
    
    return response.data
}

export async function postTopic(topic:any){
    const response = await api.post(process.env.NEXT_PUBLIC_SERVER_URL+'/api/topic', topic)
    //return response.data
}

export async function deleteTopicComment( id:any){
    await api.delete(process.env.NEXT_PUBLIC_SERVER_URL+'/api/topic/comment/'+ id)
}