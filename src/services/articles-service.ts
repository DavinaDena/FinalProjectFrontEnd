

import axios from "axios";
import api from "./token-interceptor";

export async function fetchArticles() {
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/')
    //console.log("from fetchArticles");

    return response.data

}

export async function fetchArticleById(id: any) {
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id)
    // console.log(response.data);

    return response.data
}

export async function postArticleComment(id: any, comment: any) {
    const response = await api.post(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id + '/comment', { text: comment })

    return response.data
}




export async function deleteArticle(id: any) {
    await api.delete(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id)

}


export async function postArticle (article: any){
    const response = await api.post(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' ,  article)

    return response.data
}
