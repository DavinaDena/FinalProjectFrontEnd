import api from "./token-interceptor";



export async function fetchCategories(){
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/category/')
    return response.data
}


export async function fetchCategoryById(id:any){
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/category/'+id)
    return response.data
}


// export async function fetchCategories(){
//     const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL+'/api/category/')
//     return response.data
// }