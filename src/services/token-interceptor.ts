import axios from "axios";
import { getSession } from "next-auth/react";
import Router from 'next/router'

const api = axios.create({
	baseURL: process.env.NEXT_PUBLIC_API_URL
})
api.interceptors.response.use((response) => {

	return response
}, (error) => {
	
	
	console.log(error.response);
	 //Error(error.response)
})
api.interceptors.request.use(async (config: any) => {
	
	const session = await getSession();
	// console.log(session);
	
	if (session) {

		config.headers.authorization = 'bearer ' + session.accessToken;
	}
	return config;
});



export default api
