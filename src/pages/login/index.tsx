



import type { GetServerSideProps, NextPage } from 'next'
import Head from 'next/head'
import { signIn, signOut, useSession } from "next-auth/react";


const Home: NextPage<any> = () => {

    const { data: session } = useSession()
    
    const user = session?.user as any;

    return (
        <div >
            <Head>
                <title>Blastowicz - Login</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className='background'>

                {session &&

                    //make him go to user profile!
                    <div className='text-color p-5'>
                        <p>you are logged in as {user.name} <button type="button" className="btn btn-dark" onClick={() => signOut()}>Logout</button></p>

                    </div>
                }
                {/* {session &&
                    <p>your token is {session.accessToken}</p>
                } */}

                {!session &&
                    <div className='text-color text-ali d-flex justify-content-center align-items-center'>
                        <div className='row'>
                            <div className='align-items-center'>
                                <h3 className='my-5'>Come and share your opinions!</h3>

                                <button type="button" className="btn btn-outline-danger btn-lg" onClick={() => signIn()}>Login</button>
                                <a href="/register">
                                <p className='my-5'>You don't have an account? Click here to register!</p></a>
                            </div>

                        </div>

                    </div>

                }
            </div>

        </div>
    )
}

export default Home


