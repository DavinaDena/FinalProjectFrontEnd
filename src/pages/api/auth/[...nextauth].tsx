import axios from "axios";
import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import { JWT } from "next-auth/jwt";
import { signOut } from "next-auth/react";
import jwtDecode from "jwt-decode";




async function refreshAccessToken(token: JWT) {

    try {
        if (token.refresh_token) {
            const response = await axios.post(process.env.NEXT_PUBLIC_API_URL + '/user/refreshToken', {}, {
                headers: {
                    "Authorization": `bearer ${token.refresh_token}`
                },
            })
            return {
                ...token,
                accessToken: response?.data.token,
                refreshToken: response?.data.refresh_token ?? token.refresh_token, // Fall back to old refresh token
            }
        } else {
            return token
        }




    } catch (error) {
        console.log(error)
        signOut()
        return {
            ...token,
            error: "RefreshAccessTokenError",
        }
    }
}


export default NextAuth({



    providers: [
        CredentialsProvider({
            name: "Login",
            credentials: {
                email: { label: "Email", type: "email", placeholder: "Your email..." },
                password: { label: "Password", type: "password", placeholder: "Your Password..." }
            },
            async authorize(credentials, req) {
                try {
                    const response = await axios.post("http://localhost:8000/api/user/login", credentials);
                    const data = response.data;
                    if (data.user && data.token) {
                        return data;
                    }
                    return null;
                } catch (error) {
                    console.log("error");
                    return null;
                }
            }
        }),



    ],

   secret: process.env.JWT_SECRET ,
    callbacks: {
        // async jwt({ token, user }) {
        //     if (user) {
        //         return {
        //             accessToken: user.token,
        //             user: user.user,
        //         }
        //     }


        //     return token;
        // },
        // async session({ session, token }) {
        //     session.accessToken = token.accessToken;
        //     session.user = token.user as any;
        //     return session;
        // }


        // Getting the JWT token from API response
        async jwt({ token, user }) {

            if (user) {
                return {
                    accessToken: user.token,
                    user: user.user,
                    refresh_token: user.refresh_token
                }

                // //@ts-ignore
                // token.user = user?.data?.user;
                // //@ts-ignore
                // token.accessToken = user?.data?.token
                // //@ts-ignore
                // token.refresh_token = user?.data?.refresh_token

            }
            
            //@ts-ignore
            if (token.accessToken && Date.now() / 1000 < (jwtDecode(token.accessToken).exp as number)) {

                return token
            }

            // Access token has expired, try to update it
            return refreshAccessToken(token)
        },

        async session({ session, token }) {
            session.accessToken = token.accessToken
            //@ts-ignore
            session.user = token?.user as any
            session.refresh_token = token?.refresh_token
            return Promise.resolve(session);
        }
    },

    // callbacks: {
    //     // Getting the JWT token from API response
    //     async jwt({ token, user }) {

    //         if (user !== undefined) {
    //             //@ts-ignore
    //             token.user = user?.data?.user;
    //             //@ts-ignore
    //             token.accessToken = user?.data?.token
    //             //@ts-ignore
    //             token.refresh_token = user?.data?.refresh_token

    //         }
    //         //@ts-ignore
    //         if (token.accessToken && Date.now() / 1000 < (jwtDecode(token.accessToken).exp as number)) {

    //             return token
    //         }

    //         // Access token has expired, try to update it
    //         return refreshAccessToken(token)
    //     },

    //     async session({ session, token }) {
    //         session.accessToken = token.accessToken
    //         //@ts-ignore
    //         session.user = token?.user
    //         session.refresh_token = token?.refresh_token
    //         return Promise.resolve(session);
    //     }
    // },
    // secret: process.env.JWT_SECRET,
    // theme: {
    //     colorScheme: "light"
    // },
    // pages: {
    //     signIn: '/Login',
    //     signOut: '/auth/signout',
    // },



})


// export default NextAuth({
// 	// Configure one or more authentication providers

// 	providers: [
// 		CredentialsProvider({
// 			name: "Sign-In",
// 			type: "credentials",
// 			id: "signin",
// 			credentials: {
// 				email: { label: "Email", type: "text", placeholder: "Exemple : test@gmail.com" },
// 				password: { label: "Password", type: "password" },
// 			},
// 			authorize: async (credentials) => {

// 				const { data } = await api.post("/user/login",
// 					{
// 						password: credentials?.password,
// 						email: credentials?.email
// 					})



// 				if (data) {

// 					return { status: 'success', data: data }

// 				} else {
// 					return data
// 				}

// 			}
// 		}),
// 		CredentialsProvider({
// 			name: "Sign-Up",
// 			type: "credentials",
// 			id: "signup",
// 			credentials: {
// 				username: { label: "Username", type: 'text' },
// 				email: { label: "Email", type: "text", placeholder: "Exemple : test@gmail.com" },
// 				password: { label: "Password", type: "password" },
// 			},
// 			authorize: async (credentials) => {
// 				try {
// 					const { data } = await api.post("/user/register", {
// 						username: credentials?.username,
// 						email: credentials?.email,
// 						password: credentials?.password,
// 					})

// 					if (data) {
// 						return { status: 'success', data: data }

// 					} else {
// 						return data
// 					}
// 				} catch (e: any) {

// 					throw new Error(e.response.data.error)

// 				}

// 			}
// 		}),
// 	],

// callbacks: {
// 		// Getting the JWT token from API response
// 		async jwt({ token, user }) {

//         if (user !== undefined) {
//             //@ts-ignore
//             token.user = user?.data?.user;
//             //@ts-ignore
//             token.accessToken = user?.data?.token
//             //@ts-ignore
//             token.refresh_token = user?.data?.refresh_token

//         }
//         //@ts-ignore
//         if (token.accessToken && Date.now() / 1000 < (jwtDecode(token.accessToken).exp as number)) {

//             return token
//         }

//         // Access token has expired, try to update it
//         return refreshAccessToken(token)
//     },

// 		async session({ session, token }) {
//         session.accessToken = token.accessToken
//         //@ts-ignore
//         session.user = token?.user
//         session.refresh_token = token?.refresh_token
//         return Promise.resolve(session);
//     }
// },
// secret: process.env.JWT_SECRET,
//     theme: {
//     colorScheme: "light"
// },
// pages: {
//     signIn: '/Login',
//         signOut: '/auth/signout',
// 	},




// })
