//page with all topics

import { GetServerSideProps } from "next"
import Head from "next/head"
import TopicAdd from "../../components/topicAdd"
import { TopicsList } from "../../components/topicsList"
import { fetchTopics } from "../../services/topics-services"



const Topics = ({ topics }: any) => {



    return (
         

        <div className="background ">
            <Head>
                <title>Blastowicz: Forum</title>
                <meta name="description" content="Blastowicz Home Page" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div className="container">

                <div className="py-5">
                    <div className="border-bot first-text-color">
                        <h2>Forum</h2>
                    </div>
                </div>


                <div className="d-flex justify-content-center">
                    <p >
                        <a className="btn background-button-color " data-bs-toggle="collapse" href="#collapseExample" role="button" >
                            Write a post!
                        </a>

                    </p>
                </div>
                <div className="collapse" id="collapseExample">
                    <div className="container">
                        <TopicAdd />
                    </div>
                </div>



                <div className="pb-5">
                    <TopicsList key={topics.id} list={topics} />
                </div>

            </div>

        </div>


    )
}

export default Topics

export const getServerSideProps: GetServerSideProps = async () => {
    return {
        props: {
            topics: await fetchTopics()
        }
    }
}
