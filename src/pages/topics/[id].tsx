//Page Topics by ID

import { GetServerSideProps, NextPage } from "next"
import CommentsAdd from "../../components/commentsAdd";
import { CommentsList } from "../../components/commentsList"
import {  fetchTopicById } from "../../services/topics-services"


const TopicID: NextPage<any> = ({ topic }) => {




    return (
        <div className="background text-color py-3">
            <div className="container my-5">
                <div className="background-second ">
                    <div className="row">
                        <div className="col-9 p-4 ">
                            <h2 className="px-3 border-bot mb-4 fw-bolder first-text-color">{topic.title}</h2>
                            <p className="px-5 mb-3  ">{topic.text}</p>
                            <p>{topic.user.name}</p>
                        </div>
                    </div>


                    <div className="row">
                        <CommentsAdd id={topic.id} istopic={true} />


                    </div>

                </div>




                <div className="first-text-color ">
                    <CommentsList key={topic.id} list={topic.comment} />
                </div>

            </div>


        </div>
    )
}

export default TopicID


export const getServerSideProps: GetServerSideProps = async (context) => {
    if (context.query.id) {
        return {
            props: {
                topic: await fetchTopicById(context.query.id),
            }
        }

    } return {
        notFound: true
    }
}