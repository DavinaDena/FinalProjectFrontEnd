


import type { GetServerSideProps, NextPage } from 'next'
import Head from 'next/head'
import { fetchArticles } from '../services/articles-service';
import { ArticleList } from '../components/articlesList';



const Home: NextPage<any> = ({ articles }) => {

    // const { data: session } = useSession()
    // const user = session?.user as any;

    return (
        <div >
            <Head>
                <title>Blastowicz</title>
                <meta name="description" content="Blastowicz Home Page" />
                <link rel="icon" href="/favicon.ico" />
            </Head>


            {/* background */}
            <div className='background'>
                {/* banner */}
                <div className=" background-banner">
                    <div className="container-fluid">
                        <div className='row'>
                            <div className='offset-md-6 col-md-4 '>
                                <div className="m-4">
                                    <div className="card-body background-box my-5">
                                        <h5 className="card-title my-3">Welcome</h5>
                                        <p className="card-text">This is a dedicated place for nerds and nerds news! </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div className='container'>
                    <div className=' d-flex justify-content-center align-items-center'>
                        <h2 className='latestNews m-5'>Latest News</h2>
                    </div>

                    <div className='pb-5'>
                        <ArticleList list={articles} />

                    </div>




                </div>
            </div>
        </div>

    )
}

export default Home


export const getServerSideProps: GetServerSideProps = async () => {


    return {
        props: {
            articles: await fetchArticles(),
        }
    }
}

