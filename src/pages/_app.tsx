import 'bootstrap/dist/css/bootstrap.css';
import '../styles/GeneralCSS.css'
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { useEffect } from 'react';
import { SessionProvider } from 'next-auth/react'
import Image from 'next/image'
import Link from 'next/link'
import NavBar from '../components/navBar';




function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  // Place this in the pages/_app.js file
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap");
  }, []);

  console.log(session);

  return <SessionProvider session={session}>
    <div>

      <div>
        {/* PAGE NAME */}
        <div className='d-flex justify-content-center align-items-center'>
          <h1 className='textshadow'>Blastowicz</h1>
        </div>



        {/* SOCIAL LOGOS */}
        <div className='d-flex justify-content-center align-items-center mb-3 '>
          <div className='d-none d-md-block'>
            <div className='row'>
              <div className="col-sm-3 "><Image src="/facebookBlack.png" width={20} height={20} /></div>
              <div className="col-sm-3"><Image src="/instagram.svg" width={20} height={20} /></div>
              <div className="col-sm-3"><Image src="/youtube.png" width={20} height={20} /></div>
              <div className="col-sm-3"><Image src="/twitter.png" width={20} height={20} /></div>
            </div>

          </div>
        </div>

      </div>


      <NavBar />
    </div>


    {/* PAGES CONTENT!! */}

    <Component  {...pageProps} />
    {/* PAGES CONTENT!! */}


    {/* FOOTER */}
    <div className="footer-background">
      <div className='container'>
        <div className="row py-3">
          <div className="col-md-4 d-flex justify-content-center align-items-center">
            <h4>Copyright 2022</h4>
          </div>
          <div className="col-md-4 d-flex justify-content-center">
            <Image src="/logo.jpeg" width={150} height={150} className="imglogo" />
          </div>
          <div className="col-md-4 d-flex justify-content-center align-items-center">
            <div className="row ">
              <div className=' d-flex justify-content-center align-items-center'>
                <div className="col-sm-3"><Image src="/facebookWhite.png" width={100} height={100} /></div>
                <div className="col-sm-3"><Image src="/instagram.svg" width={100} height={100} /></div>
                <div className="col-sm-3"><Image src="/youtube.png" width={100} height={100} /></div>
                <div className="col-sm-3"><Image src="/twitter.png" width={100} height={100} /></div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  </SessionProvider>

}
export default MyApp
