


//a page with all articles

import { GetServerSideProps } from "next"
import { ArticleList } from "../../components/articlesList"
import { CategoryList } from "../../components/categoryList"
import { fetchArticles } from "../../services/articles-service"
import { fetchCategories } from "../../services/category-service"


const Articles = ({articles}:any) => {

    // const result = articles.filter(articles => articles.category)
    // console.log(result.category);
    
    return (
        <div>
            {/* <CategoryList liste={categories} /> */}
            <ArticleList list={articles} />
        </div>
    )
}

export default Articles


export const getServerSideProps: GetServerSideProps = async () => {


    return {
        props: {
            articles: await fetchArticles(),
            categories: await fetchCategories()

        }
    }
}
