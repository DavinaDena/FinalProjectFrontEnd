import { GetServerSideProps, NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import CommentsAdd from "../../components/commentsAdd";
import { CommentsList } from "../../components/commentsList";
import { deleteArticle, fetchArticleById } from "../../services/articles-service";
import parse from 'html-react-parser';
import sanitizeHtml from 'sanitize-html';


const ArticleId: NextPage<any> = ({ article }) => {

    const { data: session } = useSession()
    const router = useRouter();

    const refreshData = () => {
        router.replace(router.asPath);
    }

    const user = session?.user as any;


    

    async function onClick() {
        await deleteArticle(article.id)
        refreshData()
    }


    return (

        <div className="background py-3">

            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="d-flex justify-content-center">
                            <h2 className="latestNews">{article.title}</h2>

                        </div>
                    </div>

                    <div className="col-12">
                        <div className="first-text-neutral"> {parse(sanitizeHtml(article.text))}</div>
                        <p className="first-neutral">{article.user.name}</p>
                        <p className="first-neutral">{article.date}</p>
                        {session && user.role == 'admin' && <button onClick={onClick} type="button" className="btn btn-danger">Delete</button>}
                    </div>

                    <div >
                        <CommentsAdd id={article.id} />

                    </div>
                </div>



                <div>
                    <CommentsList list={article.comment} />
                </div>


            </div>

        </div>



    )
}

export default ArticleId

export const getServerSideProps: GetServerSideProps = async (context) => {
    if (context.query.id) {
        return {
            props: {
                article: await fetchArticleById(context.query.id)
            }
        }
    } return {
        notFound: true
    }

}






