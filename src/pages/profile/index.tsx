import { useSession } from "next-auth/react"
import { useRouter } from "next/router"
import { useEffect, useState, useRef } from "react"
import api from "../../services/token-interceptor"
import { useForm } from 'react-hook-form';
import { postArticle } from "../../services/articles-service"
import { fetchCategories } from "../../services/category-service";
import { GetServerSideProps } from "next";
import React from 'react';
import Head from "next/head";

interface User {
    name?: string | undefined,
    email?: string | undefined,
    // comment?: []
}


type FormData = {
    title?: string | undefined,
    text?: string | undefined,
    category?: any,
};


const Profile = ({ categories }: any) => {

    const [user, setUser] = useState<User | null>(null)
    const { data: session } = useSession()
    const router = useRouter();
    const users = session?.user as any;
    const { register, handleSubmit } = useForm<FormData>();
    const editorRef = useRef()
    const [editorLoaded, setEditorLoaded] = useState(false)
    const { CKEditor, ClassicEditor }: any = editorRef.current || {}

    const [data, setData] = useState('');
    const [value, setValue] = useState()


    useEffect(() => {
        editorRef.current = {
            CKEditor: require('@ckeditor/ckeditor5-react').CKEditor, //Added .CKEditor
            ClassicEditor: require('@ckeditor/ckeditor5-build-classic'),
        }
        setEditorLoaded(true)
    }, []);



    const refreshData = () => {
        router.replace(router.asPath);
    }

    async function onSubmit(data: any) {
        if (value != undefined) {
            await postArticle({ ...data, text: value })
        }
    };

    useEffect(() => {
        async function fetchUserAccount() {
            const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/user/account')
            setUser(response.data)
        }
        fetchUserAccount()
    }, [])

    return (
        <div className="background">


            <Head>
                <title>Blastowicz: Profile</title>
                <meta name="description" content="Blastowicz Home Page" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className="container">

                <div className="row py-5">
                    <div className="col-12">
                        <h3 className="first-text-color"> {user?.name}</h3>

                        <div>
                            {session && users.role == 'admin' &&
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <h5 className="first-text-neutral py-3">Insert Article Title</h5>
                                    <div className="row ">
                                        <input type="text" placeholder="title" {...register("title", { required: true })} />
                                    </div>

                                    {/* <input type="text" placeholder="text" {...register("text", { required: true })} /> */}


                                    <h5 className="first-text-neutral py-3 ">Body Article</h5>
                                    <>
                                        {editorLoaded ? <CKEditor
                                            type="text" placeholder="text"
                                            editor={ClassicEditor}
                                            data={data}
                                            onReady={(editor: any) => {
                                                // You can store the "editor" and use when it is needed.
                                                console.log('Editor is ready to use!', editor);
                                            }}
                                            onChange={
                                                (e: any, editor: { getData: () => React.SetStateAction<undefined>; }) => setValue(editor.getData())

                                                // (event, editor) => {
                                                // const data = editor.getData()
                                                // setData(data);
                                                //}
                                            }
                                        /> : <p>Loading...</p>}
                                    </>



                                    <h5 className="first-text-neutral py-3 ">Choose the category</h5>
                                    <div className="input-group mb-3">
                                        <label className="input-group-text" htmlFor="inputGroupSelect01">Options</label>


                                        <select {...register("category", {})} className="form-select" id="inputGroupSelect01">

                                            {categories.map((item: any) =>



                                                <option key={item.id} value={item.id}>{item.category}</option>
                                            )}

                                        </select>
                                    </div>
                                    <input type="submit" className="btn background-button-color" />
                                </form>
                            }
                        </div>
                    </div>


                </div>
            </div>




        </div>
    )



}

export default Profile


export const getServerSideProps: GetServerSideProps = async () => {


    return {
        props: {
            categories: await fetchCategories()

        }
    }
}



// <h3> {user?.name}</h3>

// <div>
//     {session && users.role == 'admin' &&
//         <form onSubmit={handleSubmit(onSubmit)}>
//             <input type="text" placeholder="title" {...register("title", { required: true })} />


//             {/* <input type="text" placeholder="text" {...register("text", { required: true })} /> */}


//             <>
//                 {editorLoaded ? <CKEditor
//                     type="text" placeholder="text"
//                     editor={ClassicEditor}
//                     data={data}
//                     onReady={(editor: any) => {
//                         // You can store the "editor" and use when it is needed.
//                         console.log('Editor is ready to use!', editor);
//                     }}
//                     onChange={
//                         (e: any, editor: { getData: () => React.SetStateAction<undefined>; }) => setValue(editor.getData())

//                         // (event, editor) => {
//                         // const data = editor.getData()
//                         // setData(data);
//                         //}
//                     }
//                 /> : <p>Loading...</p>}
//             </>




//             <div className="input-group mb-3">
//                 <label className="input-group-text" htmlFor="inputGroupSelect01">Options</label>


//                 <select {...register("category", {})} className="form-select" id="inputGroupSelect01">

//                     {categories.map((item: any) =>



//                         <option key={item.id} value={item.id}>{item.category}</option>
//                     )}

//                 </select>
//             </div>
//             <input type="submit" />
//         </form>
//     }
// </div>