import { NextPage } from "next";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { NextResponse } from "next/server";
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { userRegister } from "../../services/user-service";

// type FormData = {
//     name: string;
//     email: string;
//     password: string;
// };

const Register = () => {
    const router = useRouter();

    const { register, handleSubmit, formState: { errors } } = useForm();
    // const onSubmit = data => console.log(data);
    // console.log(errors);


    async function onSubmit(data: any) {
        await userRegister(data)
        router.replace('/login');
    };


    return (
        <div className="background">

            <div className='text-color text-ali d-flex justify-content-center align-items-center'>
                <div className='row'>
                    <div className='align-items-center'>

                        <h3 className="my-5">Register Page</h3>

                        <div className="d-flex justify-content-center">
                            {/* <div className="col-9"> */}


                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="row  p-2">
                                        <input type="name" placeholder="name" {...register("name", { required: true, max: 20, min: 3 })} />
                                    </div>
                                    <div className="row  p-2">
                                        <input type="email" placeholder="email" {...register("email", { required: true })} />

                                    </div>
                                    <div className="row p-2">
                                        <input type="password" placeholder="password" {...register("password", { required: true })} />
                                    </div>
                                    <div className="row mx-5 pb-5 p-2">

                                        <button type="submit" className="btn background-button-color btn-lg">Register</button>
                                        {/* <input type="submit" /> */}
                                    </div>
                                </form>
                            </div>
                        </div>




                    </div>
                </div>
            </div>


        // </div>

    )








}

export default Register


