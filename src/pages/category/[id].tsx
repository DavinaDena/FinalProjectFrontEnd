import { GetServerSideProps, NextPage } from "next"
import { ArticleList } from "../../components/articlesList"
import {  fetchCategoryById } from "../../services/category-service"


const CategoryId: NextPage<any> = ({ category }: any) => {

    return (
        <div className="background">
            <div className="container-fluid">
                <div className="d-flex justify-content-center">
                <h3 className="latestNews my-3" >{category.category}</h3>

                </div>
                <div className="pb-5">
                <ArticleList list={category.article} />

                </div>
            </div>

        </div>
    )
}

export default CategoryId


export const getServerSideProps: GetServerSideProps = async (context: any) => {
    if (context.query.id) {
        return {
            props: {
                category: await fetchCategoryById(context.query.id)
            }
        }

    } return {
        notFound: true
    }
}