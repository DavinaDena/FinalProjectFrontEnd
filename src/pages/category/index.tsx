import { GetServerSideProps } from "next"
import Head from "next/head"
import { ArticleList } from "../../components/articlesList"
import { CategoryList } from "../../components/categoryList"
import { fetchArticles } from "../../services/articles-service"
import { fetchCategories } from "../../services/category-service"



const Category = ({ categories, articles }: any) => {

    // const result = articles.filter(articles => articles.category)
    // console.log(result.category);

    return (
        <div className="background">
             <Head>
                <title>Blastowicz: Articles</title>
                <meta name="description" content="Blastowicz Home Page" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <div className="container-fluid">
                <div className="pb-5">
                    <div>
                        <CategoryList key={categories.id} data={categories} />

                    </div>
                    <div>
                        <ArticleList key={articles.id} list={articles} />

                    </div>
                </div>

            </div>

        </div>
    )
}

export default Category




export const getServerSideProps: GetServerSideProps = async () => {


    return {
        props: {
            articles: await fetchArticles(),
            categories: await fetchCategories()

        }
    }
}
