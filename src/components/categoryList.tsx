import { CategoryButton } from "./categoryButton"




export const CategoryList= ({data}:any)=>{



    return(
        <div className="row">
            {data.map((item:any)=>
                <CategoryButton key={item.id} category={item}/>
            )}
        </div>
    )
}