import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { deleteTopicComment } from "../services/topics-services";



export const CommentsCard = ({ comments }: any) => {
    const { data: session } = useSession()
    const router = useRouter();

    const refreshData = () => {
        router.replace(router.asPath);
    }

    const user = session?.user as any;

    

    async function onClick() {
        await deleteTopicComment(comments.id)
        refreshData()
    }



    return (
        <div className="py-4 px-5">

            <div className="row border-bot ">
                <div className="col-9 py-3">
                    <p className="text-color">{comments.text}</p>

                </div>
                <div className="col-3">
                    <p className="fw-bolder third-text-color">{comments.date}</p>
                    <p className="third-text-color">{comments.user.name}</p>
                    {session && user.id == comments.user.id  && user.role == 'admin' && <button onClick={onClick} type="button" className="btn btn-danger">Delete</button>}
                </div>


            </div>
        </div>

    )
}