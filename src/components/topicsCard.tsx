


import Link from 'next/link'


export const TopicCard = ({ topics }: any) => {



    return (
        <div className="border-bot text-color">

            <div className='row'>
                <div className='col-9'>
                    <div className='row'>
                        <div className='col-9'>
                            <h3 className='first-text-color'>{topics.title}</h3>
                        </div>
                    </div>

                    <p className='third-text-neutral'>{topics.text}</p>
                </div>
                <div className='col-3'>
                    <p>{topics.date}</p>
                    <p>{topics.user.name}</p>
                </div>

            </div>

            <div className='py-3 d-flex justify-content-center'>
                <Link href={'/topics/' + topics.id}>
                    <button type="button" className="btn background-button-color ">Keep Reading</button>
                    {/* <a > See Full</a> */}
                </Link>
            </div>



        </div>
    )
}