
import Link from 'next/link'
import parse from 'html-react-parser';
import sanitizeHtml from 'sanitize-html';

export const ArticleCard = ({ article }: any) => {
    
    return (

        <div className='col-md-4 try'>

            <div className="card card-background  m-2" >
                {/* <Image src="..." className="card-img-top" alt="..."/> */}
                <div className="card-body">

                    <h5 className="card-title otherborder superbold text-center ">{article.title}</h5>


                    <div className="card-text line-clamp first-text-neutral "> { parse(sanitizeHtml(article.text))}</div>
                    
                    <div className=' d-flex justify-content-center '>
                        <Link href={'/articles/' + article.id}>
                            <a type="button"  className='otherbold'>Read More!</a>
                        </Link>
                    </div>

                </div>
            </div>

        </div>

    )
}

