
import { CommentsCard } from "./commentsCard"


export const CommentsList = ({ list }: any) => {


    return (
        <div>
            {list.map((item: any) => 
                <CommentsCard key={item.id} comments={item} />
            )}
        </div>
    )
}