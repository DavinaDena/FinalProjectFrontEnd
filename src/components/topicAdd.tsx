
import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import React from 'react';
import { useForm } from 'react-hook-form';
import { postTopic } from '../services/topics-services';

type FormData = {
    title: string;
    text: string;
};


function TopicAdd() {
    const { data: session } = useSession()
    const router = useRouter();
    const { register, handleSubmit } = useForm<FormData>();


    const refreshData = () => {
        router.replace(router.asPath);
    }

    async function onSubmit(data: any) {

        await postTopic(data)

        refreshData()
    }
    if (session) {

        return (
            <div className='mx-5'>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className='row mx-5 p-2'>
                        <input type="text" placeholder="title" {...register("title", { required: true })} />

                    </div>
                    <div className='row mx-5 p-2'>
                        <input type="text" placeholder="text" {...register("text", { required: true })} />

                    </div>
                    <div className="row mx-5 pb-5 p-2">
                    <input type="submit" />
                    </div>
                </form>
            </div>
        )
    }
    return (
        <a href="/api/auth/signin">Sign in to interact with us!</a>
    )
}

export default TopicAdd