import { TopicCard } from "./topicsCard"



export const TopicsList=({list}:any)=>{

    return(
        <div className="background-second" >
            {list.map((item:any)=>
            <div className="p-5">
                
                <TopicCard  topics={item}  />
            </div>
            )}
        </div>
    )
}