import { useSession } from 'next-auth/react'
import Image from 'next/image'
import Link from 'next/link'


const NavBar = () => {
  const { data: session } = useSession()

  return (
    <div>
      <nav className="navbar navbar-expand-md navbar-dark  bg-dark ">
        <div className="container-fluid justify-content-end">
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>


          {/* DROPDOWN */}

          {/* <Link href={'/login'}>

          <a className="nav-link"> 
          <Image src="/user.png" width={25} height={25} />
              </a></Link> */}
          <div className="dropdown">

            <a className="nav-item dropdown-toggle mb-1" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              <Image src="/user.png" width={25} height={25} />
            </a>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              {session && <Link href={'/login'}>
                <li><a className="dropdown-item" href="#">Logout</a></li>
              </Link>}

              {!session && <div>
                <Link href={'/login'}>
                  <li><a className="dropdown-item" href="#">Login</a></li>
                </Link>

                <Link href={'/register'}>
                  <li><a className="dropdown-item" href="#">Register</a></li>
                </Link>
              </div>
              }

            </ul>
          </div>


          <div className="collapse navbar-collapse navedit justify-content-center " id="navbarSupportedContent">
            <ul className="navbar-nav  mb-2 mb-md-0 justify-content-center">
              <li className="nav-item ">
                <Link href={'/'}>
                  <a className="nav-link active first-neutral nav-font" aria-current="page" href="#">Home</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href={'/category'}>
                  <a className="nav-link first-color nav-font">Articles</a>
                </Link>

              </li>
              <li className="nav-item">
                <Link href={'/topics'}>
                  <a className="nav-link second-color nav-font" >Forum</a>
                </Link>
              </li>
              {session &&
                <li className="nav-item">
                  <Link href={'/profile'}>
                    <a className="nav-link third-color nav-font">Profile</a>
                  </Link>
                </li>}


            </ul>
          </div>




        </div>
      </nav>
    </div>
  )
}

export default NavBar