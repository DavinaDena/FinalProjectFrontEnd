
import { useSession } from "next-auth/react";
import { postTopicComment } from "../services/topics-services"
import { useForm } from "react-hook-form";
import { useRouter } from 'next/router';
import { postArticleComment } from "../services/articles-service";

type FormData = {
    comment: string;
};

function CommentsAdd({ istopic = false, id }: any) {

    const { data: session } = useSession()
    const router = useRouter();
    const { register, handleSubmit } = useForm<FormData>();

    const refreshData = () => {
        router.replace(router.asPath);
    }

    async function onSubmit(data: any) {
        if (istopic) {
            await postTopicComment(id, data.comment)
        } else {
            await postArticleComment(id, data.comment)
        }
        refreshData()
    }


    if (session) {
        return (
            <div>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="row mx-5 p-2">

                    <textarea {...register("comment", {})} />


                    </div>

                    <div className="row mx-5 pb-5 p-2">
                    
                        <input type="submit"  className="btn background-button-color"/>

                    </div>
                </form>


            </div>


        )
    }


    return (
        <a href="/api/auth/signin">Sign in to interact with us!</a>
    )
}

export default CommentsAdd


