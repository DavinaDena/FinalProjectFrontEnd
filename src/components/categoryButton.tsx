import Link from "next/link"






export const CategoryButton = ({ category }: any) => {

    return (
        <div className="col-3 d-flex justify-content-center">
            <div className="my-3">
                <Link href={'/category/' + category.id}>
                <button type="button" className="btn background-button-color" >{category.category}</button>

            </Link>
            </div>
            
        </div>
    )
}